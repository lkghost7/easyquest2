﻿using UnityEngine;

[CreateAssetMenu(menuName = "State")]
public class State : ScriptableObject
{
    
    [TextArea(10, 25)] public string storyText;
    [TextArea(10, 25)] public string textVibor;
    public string btn1;
    public string btn2;
    public string btn3;
    public string btn4;
    
    public Sprite imageMain;
    public State state1;
    public State state2;
    public State state3;
    public State state4;
    public int win;
    public int ofBtn;
}
