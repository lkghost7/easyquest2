﻿using System;

public class PressBtn : GenericSingletonClass<PressBtn>
{
    public Action GetBtn1 = delegate {};
    public Action GetBtn2 = delegate {};
    public Action GetBtn3 = delegate {};
    public Action GetBtn4 = delegate {};


    public void btn1() {
        print("btn1");
        GetBtn1();
    }
    
    public void btn2() {
        print("btn2");
        GetBtn2();
    }
    
    public void btn3() {
        print("btn3");
        GetBtn3();
    }
    
    public void btn4() {
        print("btn4");
        GetBtn4();
    }
    
}
